
//////////////////////////////////////////////////////////////////////////////
Set Bin.Statistics(Anything actual, Anything prediction, Real threshold, 
  Set statistics)
//////////////////////////////////////////////////////////////////////////////
{
  Real nError = Copy(NError);
  @Matrix act = If(Grammar(actual)=="Matrix", @Matrix(actual), actual);
  @Matrix prd = If(Grammar(prediction)=="Matrix", @Matrix(prediction), 
    prediction);
  If(nError==NError, {
    Matrix pred = Min(Max($prd, 0), 1); // TEST!
    Matrix esti = GE(pred, Rand(Rows(pred), 1 , threshold, threshold));
    // Values (P: Positive [1], N: Negative [0])
    // Actual values: (A: Actual)
    Real AS = Rows($act); // (S: Size)
    //  AS = AP + AN
    Real AP = MatSum($act);
    Real AN = AS - AP;
    // Predicted values (P: Predicted)
    //  AS = PP + PN
    Real PP = MatSum(esti); 
    Real PN = AS - PP;
    // Trues and Falses (T: True, F: False)
    //  TP + FN = AP,  TN + FP = AN
    //  TP + FP = PP,  TN + FN = PN
    Real TP = MatSum(And(esti, $act));
    Real FN = AP - TP;
    Real FP = PP - TP;
    Real TN = AN - FP;
    EvalSet(statistics, Real (Text statistic) { 
      Case(
      statistic<:[["AS","ActualSize","Size","S"]], AS,
      statistic<:[["AP","ActualPositive","Positive","P"]], AP,
      statistic<:[["AN","ActualNegative","Negative","N"]], AN,
      statistic<:[["PP","PredictedPositive","P'"]], PP,
      statistic<:[["PN","PredictedNegative","N'"]], PN,
      statistic<:[["TP","TruePositive"]], TP,
      statistic<:[["FN","FalseNegative"]], FN,
      statistic<:[["FP","FalsePositive"]], FP,
      statistic<:[["TN","TrueNegative"]], TN,
      // Statistics
      //  * True Positive Rate (TPR) o Sensitivity
      statistic<:[["TPR","Sensitivity","Recall"]], TPR = TP/AP,
      //  * False Positive Rate
      statistic<:[["FPR","FallOut"]], FPR = FP/AN, // 1-TPR
      //  * True Negative Rate (TNR) o Specifity
      statistic<:[["TNR","SPC","Specifity"]], TNR = TN/AN,
      //  * False Negative Rate (FNR)
      statistic == "FNR", FNR = FN/AP,
      //  * Positive Predictive Value (PPV)
      statistic<:[["PPV","Precision"]], PPV = TP/PP,
      //  * Negative Predictive Value (NPV)
      statistic == "NPV", NPV = TN/PN,
      //  * False Discovery (Acceptance) Rate (FDR)
      statistic<:[["FDR","FAR"]], FDR = FP/PP,
      //  * False Rejection Rate (FRR)
      statistic == "FRR", FRR = FN/PN,
      //  * Accuracy (ACC)
      statistic<:[["ACC","Accuracy"]], ACC = (TP+TN)/AS,
      //  * Matthews correlation coefficient (MCC)
      statistic == "MCC", MCC = (TP*TN - FP*FN)/Sqrt(AP*AN*PP*PN),
      //  * F1 Score
      statistic<:[["F1","F1Score"]], F1 = 2*TP/(AP+PP),
      //  * Improvement
      statistic<:[["I1","Improvement"]], I1 = TP*AS/AP/PP,
      // Otros estad�sticos indepndientes del umbral (threshold)
      //  * ROC.AUC
      statistic<:[["AUC","ROC.AUC"]], ROC.AUC(act, prd),
      //  * Gini Coefficient (G1)
      statistic<:[["G1","Gini"]], Bin.Gini(act, prd),
      //  * Log-Likelihood (Bernouilli distribution)
      statistic<:[["LL","LogLikelihood"]], Bin.LogLikelihood(act, prd),
      //  * Normalized Error (Normalized Sum of Pearson Residuals)
      statistic<:[["NE","NormalizedError"]], Bin.NormalizedError(act, prd),
      True, {
        WriteLn("Estad�stico '"<<statistic<<"' desconocido.", "W");
      ? })
    })
  }, {
    WriteLn("Los argumentos 'actual' y 'prediction' han de ser matrices "
      "o estructuras de tipo @Matrix.", "E");
    For(1, Card(statistics), Real (Real i) { ? })
  })
};

//////////////////////////////////////////////////////////////////////////////
Real Bin.Statistic(Anything actual, Anything prediction, Real threshold, 
  Text statistic)
//////////////////////////////////////////////////////////////////////////////
{ Bin.Statistics(actual, prediction, threshold, [[statistic]])[1] };

//////////////////////////////////////////////////////////////////////////////
Real Bin.TruePositive(Anything actual, Anything prediction, Real threshold)
{ Bin.Statistic(actual, prediction, threshold, "TruePositive") };
//////////////////////////////////////////////////////////////////////////////
Real Bin.FalseNegative(Anything actual, Anything prediction, Real threshold)
{ Bin.Statistic(actual, prediction, threshold, "FalseNegative") };
//////////////////////////////////////////////////////////////////////////////
Real Bin.FalsePositive(Anything actual, Anything prediction, Real threshold)
{ Bin.Statistic(actual, prediction, threshold, "FalsePositive") };
//////////////////////////////////////////////////////////////////////////////
Real Bin.TrueNegative(Anything actual, Anything prediction, Real threshold)
{ Bin.Statistic(actual, prediction, threshold, "TrueNegative") };
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
// TPR = TP/AP = 1 - FNR     ,     FNR = FN/AP
// FPR = FP/AN = 1 - TNR     ,     TNR = TN/AN = SPC
//////////////////////////////////////////////////////////////////////////////
Real Bin.Sensitivity(Anything actual, Anything prediction, Real threshold)
{ Bin.Statistic(actual, prediction, threshold, "Sensitivity") };
Real Bin.TPR(Anything actual, Anything prediction, Real threshold)
{ Bin.Statistic(actual, prediction, threshold, "TPR") };
Real Bin.FNR(Anything actual, Anything prediction, Real threshold)
{ Bin.Statistic(actual, prediction, threshold, "FNR") };
//////////////////////////////////////////////////////////////////////////////
Real Bin.Specifity(Anything actual, Anything prediction, Real threshold)
{ Bin.Statistic(actual, prediction, threshold, "Specifity") };
Real Bin.TNR(Anything actual, Anything prediction, Real threshold)
{ Bin.Statistic(actual, prediction, threshold, "TNR") };
Real Bin.FPR(Anything actual, Anything prediction, Real threshold)
{ Bin.Statistic(actual, prediction, threshold, "FPR") };

//////////////////////////////////////////////////////////////////////////////
// PPV = TP/PP = 1 - FDR     ,     FDR = FP/PP
// NPV = TN/PN = 1 - FRR     ,     FRR = FN/PN
//////////////////////////////////////////////////////////////////////////////
Real Bin.Precision(Anything actual, Anything prediction, Real threshold)
{ Bin.Statistic(actual, prediction, threshold, "Precision") };
Real Bin.PPV(Anything actual, Anything prediction, Real threshold)
{ Bin.Statistic(actual, prediction, threshold, "PPV") };
Real Bin.FDR(Anything actual, Anything prediction, Real threshold)
{ Bin.Statistic(actual, prediction, threshold, "FDR") };
//////////////////////////////////////////////////////////////////////////////
Real Bin.NPV(Anything actual, Anything prediction, Real threshold)
{ Bin.Statistic(actual, prediction, threshold, "NPV") };
Real Bin.FRR(Anything actual, Anything prediction, Real threshold)
{ Bin.Statistic(actual, prediction, threshold, "FRR") };
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Real Bin.Accuracy(Anything actual, Anything prediction, Real threshold)
{ Bin.Statistic(actual, prediction, threshold, "Accuracy") };
//////////////////////////////////////////////////////////////////////////////
Real Bin.MCC(Anything actual, Anything prediction, Real threshold)
{ Bin.Statistic(actual, prediction, threshold, "MCC") };
//////////////////////////////////////////////////////////////////////////////
Real Bin.F1Score(Anything actual, Anything prediction, Real threshold)
{ Bin.Statistic(actual, prediction, threshold, "F1Score") };
//////////////////////////////////////////////////////////////////////////////
// La mejora (improvement) se define como el cociente entre el ratio
// de verdaderos positivos (TPR) y el ratio de predichos positivos
//  I1 = (TP/AP) / (PP/AS)
Real Bin.Improvement(Anything actual, Anything prediction, Real threshold)
{ Bin.Statistic(actual, prediction, threshold, "Improvement") };
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Set Bin.ContingencyTable(Anything actual, Anything prediction, Real threshold)
//////////////////////////////////////////////////////////////////////////////
{
  Set stats = Bin.Statistics(actual, prediction, threshold, 
    [["TP", "FP", "FN", "TN"]]);
  Set ContingencyTable = [[ 
    Set PredictedPositive = { [[ 
      Real ActualPositive = stats[1], 
      Real ActualNegative = stats[2] ]] }, 
    Set PredictedNegative = { [[ 
      Real ActualPositive = stats[3], 
      Real ActualNegative = stats[4] ]] }
  ]]
};

//////////////////////////////////////////////////////////////////////////////
// LogLikelihood:
// Log(L) = Sum_i( Y_i�Log(P_i) + (1-Y_i)�Log(1-P_i) )
Real Bin.LogLikelihood(Anything actual, Anything prediction)
//////////////////////////////////////////////////////////////////////////////
{
  // LL = Sum_i( a_i*Log(p_i) + (1-a_i)*Log(1-p_i) )
  @Matrix act = If(Grammar(actual)=="Matrix", @Matrix(actual), actual);
  @Matrix prd = If(Grammar(prediction)=="Matrix", @Matrix(prediction), 
    prediction);
  LL = MatSum(IfMat($act, Log($prd), Log(-$prd+1)))
};

//////////////////////////////////////////////////////////////////////////////
// Se define el "error normalizado" como la suma de de los residuos de Pearson 
// (normalizados con media 0 y varianza 1) normalizada con la ra�z cuadrada 
// del n�mero de residuos. 
Real Bin.NormalizedError(Anything actual, Anything prediction)
//////////////////////////////////////////////////////////////////////////////
{
  // Residuals_i = Y_i-P_i
  // ResidualsPearson_i = (Y_i-P_i)/Sqrt(P_i�(1-P_i))
  // NE = Sum_i((Y_i-P_i)/Sqrt(P_i�(1-P_i))) / Sum_i(1)
  @Matrix act = If(Grammar(actual)=="Matrix", @Matrix(actual), actual);
  @Matrix prd = If(Grammar(prediction)=="Matrix", @Matrix(prediction), 
    prediction);
  Real NE = MatSum(($act - $prd) $/ Sqrt($prd $* (-$prd+1)))/Sqrt(Rows($act))
};

//////////////////////////////////////////////////////////////////////////////
