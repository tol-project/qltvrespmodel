/* -*- mode: C++ -*- */
////////////////////////////////////////////////////////////////////////////////
// FILE: test.tol
// Estimates logit example explained in
// https://stats.idre.ucla.edu/r/dae/logit-regression/
////////////////////////////////////////////////////////////////////////////////

//Sets the bugzilla identifier number, your e-mail and the bug�s summary
  Text email_ = "vdebuen@tol-project.org"; 
  Text summary_="Example 0003 of use of package QltvRespModel";
  Text link_ = "";

  Real numErr0 = Copy(NError);
  Real numWar0 = Copy(NWarning);


#Require QltvRespModel;


///////////////////////////////////////////////////////////////////////////////
//Definici�n del modelo
///////////////////////////////////////////////////////////////////////////////
QltvRespModel::@WgtLogit wLogit = 
[[
  Set var = { Include("var.oza") };
  //Output
  Text output.name = "admit";
  VMatrix y = Mat2VMat(var::admit);

  //Inputs
  Set inputDef = {
    VMatrix zero = Constant(VRows(y),1,0);
    VMatrix intercept = zero+1;
    VMatrix rank = Mat2VMat(var::rank);
    [[
      VMatrix intercept,
      VMatrix gre = Mat2VMat(var::gre),
      VMatrix gpa = Mat2VMat(var::gpa),  
      VMatrix rank_2=EQ(rank,intercept*2),
      VMatrix rank_3=EQ(rank,intercept*3),
      VMatrix rank_4=EQ(rank,intercept*4)
    ]]
  };
  Set input.name = EvalSet(inputDef,Name);
  VMatrix X = Group("ConcatColumns", inputDef);

  //Informaci�n a priori y de dominio
  Set prior =  [[
                                //Name        nu sig  min  max            
    BysPrior::@PsbTrnNrmUnfSclDst("intercept", ?, ?, -100, 100),
    BysPrior::@PsbTrnNrmUnfSclDst("gre",       ?, ?,    0, 100),
    BysPrior::@PsbTrnNrmUnfSclDst("gpa",       ?, ?,    0, 100),
    BysPrior::@PsbTrnNrmUnfSclDst("rank_2",    ?, ?, -100,   0),
    BysPrior::@PsbTrnNrmUnfSclDst("rank_3",    ?, ?, -100,   0),
    BysPrior::@PsbTrnNrmUnfSclDst("rank_4",    ?, ?, -100,   0) ]]
  
]];

/* */
Real wLogit::check(?);

NameBlock gradient_check = wLogit::test_gradient_log_lk_target(
  Col(0.0001,0.0001,0.0001,-.0001,-.0001,-.0001));

/* */

///////////////////////////////////////////////////////////////////////////////
//Estimaci�n m�ximo-veros�mil
///////////////////////////////////////////////////////////////////////////////
NameBlock mlh = wLogit::find_max_likelihood(10000);

NameBlock bys = wLogit::bayesian_estimation(10000);

Matrix compareCumulativeGains =
  SetMat(mlh::_.contrast::_.cumulativeGains) | 
  SubCol(SetMat(bys::_.contrast::_.cumulativeGains),[[3]]);

/* */

VMatrix mlh.beta = mlh::_.evaluation::_.beta;
VMatrix bys.beta = bys::_.evaluation::_.beta;
VMatrix dif.beta = mlh.beta-bys.beta;

VMatrix mlh.cov = mlh::_.param_cross::_.cov;
VMatrix bys.cov = bys::_.param_cross::_.cov;

VMatrix mlh.cov.L = CholeskiFactor(mlh.cov,"X");
VMatrix bys.cov.L = CholeskiFactor(bys.cov,"X");

VMatrix mlh.eps = CholeskiSolve(mlh.cov.L,dif.beta,"PtL");
VMatrix bys.eps = CholeskiSolve(bys.cov.L,dif.beta,"PtL");

Matrix mlh.test = (-F01(VMat2Mat(Abs(mlh.eps)))+1)*2;
Matrix bys.test = (-F01(VMat2Mat(Abs(bys.eps)))+1)*2;

Real mlh.test.min = MatMin(mlh.test);
Real bys.test.min = MatMin(bys.test);

Real test.min = Min(mlh.test.min, bys.test.min);

  Real numErr1 = Copy(NError);
  Real numWar1 = Copy(NWarning);

  Set partialResults_ = [[ numErr0, numErr1, mlh.test.min, mlh.test.min]];

//This is a messure of the success of the test 
  Real quality_ = And(numErr0== numErr1)*test.min;

//Return the results 
  Set resultStr_ = @strTestStatus(summary_, link_, quality_,
                    "Partial results = "<<partialResults_,
                    "NO DBServerType", "NO DBServerHost", "NO DBAlias",
                    email_);
  WriteLn(""<<resultStr_);
  resultStr_;

/* */


